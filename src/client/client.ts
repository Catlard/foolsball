//get libraries/tools
import * as THREE from 'three';
import { Controller } from './mvc/Controller';
import { Context } from "./mvc/helpers/Context";

//make scene + camera
const scene = new THREE.Scene()
const camera = new THREE.PerspectiveCamera(
    75,
    window.innerWidth / window.innerHeight,
    0.1,
    1000
);

//make renderer, add to page, adjust when changing size of window.
const renderer = new THREE.WebGLRenderer();
renderer.setSize(window.innerWidth, window.innerHeight);
document.body.appendChild(renderer.domElement);

window.addEventListener('resize', onWindowResize, false);
function onWindowResize() : void {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize(window.innerWidth, window.innerHeight);
    render();
}

//make a context!
let context = new Context(camera, scene, renderer);
context.showHelpers = true;

//the central update loop
function animate() : void {
    for (const update of context.updateFunctions) update();
    requestAnimationFrame(animate);
    render();
}
function render() : void {
    renderer.render(scene, camera);
}

//start the controller up using the context
let controller = new Controller(context);
controller.begin();

//kick 'er off! everything starts updating now.
animate();

