import { Context } from "./helpers/Context"
import { Model } from "./Model"
import { View } from "./View"

export class Controller {

    context : Context;

    constructor(context : Context) {
        this.context = context;
    }
    
    //make other objects in context, add them in.
    begin() : void {
        this.context.model = new Model(this.context);
        this.context.view = new View(this.context);
        this.init();
    } 

    init() : void {
        this.context.model.init();
        this.context.view.init();

        this.context.model.preloadModels([ 
            "./models/bleachers.obj"
        ], this.buildWorld)
    }

    

    buildWorld() : void {

        this.context.view.makeGrass();
        this.context.view.makeSkybox();
        this.context.view.makeBleachersOnEdgeOfField();
    }




}






