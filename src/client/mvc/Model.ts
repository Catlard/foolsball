import { Context } from "./helpers/Context"
import { OBJLoader } from 'three/examples/jsm/loaders/OBJLoader'
import * as THREE from 'three'; //JEFF: any cost for having three imported all over the place? This is only to create a backup mesh. is any cost to having multiple imports for this erased by webpack? 

export class Model {

    context : Context;
    modelLookup : Map<string, THREE.Group>
    backupGroupInCaseOfError : THREE.Group;

    
    constructor(context : Context) {
        this.context = context;
    }
    
    
    init() : void {

        //make a backup in case loading a group fails
        this.backupGroupInCaseOfError = new THREE.Group();
        let geometry = new THREE.BoxGeometry(1,1,1);
        let material = new THREE.MeshBasicMaterial({color: "#ff0000"});
        let mesh = new THREE.Mesh(geometry,material);
        this.backupGroupInCaseOfError.add(mesh);
    }

    //get all the models sequentially into memory so we can use them synchronously in the view.
    preloadModels(modelPaths : string[], onCompletelyLoaded : Function) : void {
        let loadIndex = 0;
        let model = this;
        let modelLookup = new Map<string, THREE.Group>();

        //finish a load. if all done, then escape with onCompletelyLoaded. Otherwise, go to the next one.
        let onLoaded = function(group : THREE.Group): void {
            modelLookup.set(modelPaths[loadIndex], group);
            console.log("ONLOADED " + modelPaths[loadIndex] + ". NOW " + loadIndex + " of " + modelPaths.length);

            loadIndex++;
            
            if(loadIndex >= modelPaths.length) { 
                console.log("COMPLETE!")
                onCompletelyLoaded();
            } else {
                model.preloadOBJ(modelPaths[loadIndex], onLoaded);
            }
        }

        this.modelLookup = modelLookup;


        model.preloadOBJ(modelPaths[loadIndex], onLoaded); //start the recursive chain
    }

    //JEFF: should this function crash?
    preloadOBJ(path : string, cb : (mesh : THREE.Group) => void) : void {
        const objLoader = new OBJLoader()
        objLoader.load(
            path, cb,
            (xhr) => { 
                console.log(path + "-->" + (xhr.loaded / xhr.total) * 100 + '% loaded') 
                cb(this.backupGroupInCaseOfError);
            },
            (error) => { 
                console.log(path + "-->" + error) 
                cb(this.backupGroupInCaseOfError);

            }
        )
    }
    


}