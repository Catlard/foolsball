import { Context } from "./helpers/Context"
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls'
import * as THREE from 'three';

export class View {

    //these configure the playing field
    fieldSize : THREE.Vector2 = new THREE.Vector2(15, 30);

    //assigned later
    context : Context;
    grass : THREE.Mesh;

    constructor(context : Context) {
        this.context = context;
    }
    
    //set up the camera
    init() : void {
        let scene = this.context.scene;
        if(this.context.showHelpers) scene.add(new THREE.AxesHelper(5))

        const controls = new OrbitControls(this.context.camera, this.context.renderer.domElement)
        controls.enableDamping = true
        this.context.subscribeUpdater(controls.update);
        console.log("Initted");

        this.context.camera.position.set(0, 3, 3);
    }

    makeSkybox() : void {
        this.context.scene.background = new THREE.CubeTextureLoader()
            .setPath( 'img/skyboxes/Teide/' )
            .load( [
                'posx.jpg',
                'negx.jpg',
                'posy.jpg',
                'negy.jpg',
                'posz.jpg',
                'negz.jpg'
            ] );
    }


    makeBleachersOnEdgeOfField() : void {
       
    }

    //make a plane
    makeGrass() : void {
        let scene = this.context.scene;
        
        //a little light never hurt anyone
        const light = new THREE.PointLight(0xffffff, 2)
        light.position.set(0, 2, 0)
        scene.add(light)
        if(this.context.showHelpers) scene.add(new THREE.PointLightHelper(light))

        //plane with normal map.
        const planeGeometry = new THREE.PlaneGeometry(this.fieldSize.x, this.fieldSize.y)
        const material = new THREE.MeshPhongMaterial()

        const texture = new THREE.TextureLoader().load('img/grass/grass.jpg')
        texture.wrapS = THREE.RepeatWrapping;
        texture.wrapT = THREE.RepeatWrapping;
        texture.repeat.set(this.fieldSize.x, this.fieldSize.y);
        material.map = texture;
        

        const normalTexture = new THREE.TextureLoader().load(
            'img/grass/grass_norm.jpg'
        );
        normalTexture.wrapS = THREE.RepeatWrapping;
        normalTexture.wrapT = THREE.RepeatWrapping;
        normalTexture.repeat.set(this.fieldSize.x, this.fieldSize.y);
        material.normalMap = normalTexture;
        
        material.normalScale.set(2, 2);
        
        this.grass = new THREE.Mesh(planeGeometry, material);
        scene.add(this.grass);

        this.grass.rotation.x = -Math.PI/2;
    }


}