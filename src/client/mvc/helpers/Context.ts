/* Allows the passing of important objects around the code, after
their creation in client.ts.*/

import * as THREE from 'three';
import { Model } from "../Model"
import { View } from "../View"


export class Context {

    //three environment
    camera : THREE.Camera;
    scene : THREE.Scene;
    renderer : THREE.WebGLRenderer;

    //mvc environment
    model : Model;
    view: View;

    //debug
    showHelpers : Boolean;

    //a list of update functions!
    updateFunctions : Array<Function> = [];

    //subscription pattern for update functions
    subscribeUpdater(f : Function) : void {
        this.updateFunctions.push(f);
    }

    //take it out if it's there.
    unsubscribeUpdater(f : Function) : boolean {
        const index = this.updateFunctions.indexOf(f);
        if(index < 0) {
            console.log("WARNING: Tried to remove an update function which wasn't subscribed? Weird.");
            return false;
        }
        else this.updateFunctions.splice(index, 1);
        return true;
    }

    //make a new one.
    constructor(camera : THREE.Camera, scene : THREE.Scene, renderer : THREE.WebGLRenderer) {
        this.camera = camera;
        this.scene = scene;
        this.renderer = renderer;
    }


}






